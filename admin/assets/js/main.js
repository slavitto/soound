
var d = document;
var itemsContainer = d.querySelectorAll('.itemsContainer')[0];
var itemRow, item;


function wordsEdit() {

	while (itemsContainer.firstChild) {
	    itemsContainer.removeChild(itemsContainer.firstChild);
	}
	//itemsContainer.innerHTML = '';

	fetch('/admin/api.php?_='+Math.random(1000)+'&action=words')
	 .then(res => res.json())
	 .then(res => {

	 	var text = res.data,
	 		word;

	 	/* Attention! Not working correctly! ******/
	 	// itemsContainer.appendChild(_el('a', 'Update All', { style: 'display: inline-block; color: #FFF; background: #C00; margin: 3px; padding: 3px 10px; border-radius: 3px;', onclick: 'clickFromBreak' }))

	 	for(var x = 0; x < text.length; x++) {

	 		itemRow = d.createElement('div');
	 		itemsContainer.appendChild(itemRow);

	 		word = encodeURIComponent(text[x]['words']);

	 		Object.assign(text[x], { 
	 			del: '<a onclick=wordDelete("' + text[x]['id'] + '") class="">Delete</a>', 
	 			upd: '<a onclick=wordFullUpdate("' + word + '") class="w_update_all">Update</a>' 
	 		});

	 		Object.keys(text[x]).forEach(function (key) {

	 			item = _el('div', '', { 'class': 'item item_w' })
			   	itemRow.appendChild(item);

			   	if(key === 'words' || key === 'words_en') {

			   		item.innerHTML = '<a onclick=soundsEdit("'+text[x].id+'")>'+text[x][key]+'</a>';

			   	} else  item.innerHTML = text[x][key];
			});

			   	

	 	}
	 })
	 .catch(err => c(err))

}


function soundsEdit(id, start = 0) {

	while (itemsContainer.firstChild) {
	    itemsContainer.removeChild(itemsContainer.firstChild);
	}
	//itemsContainer.innerHTML = '';

	fetch('/admin/api.php?_='+Math.random(1000)+'&action=word&id='+id)
	 .then(res => res.text())
	 .then(word => {

	 	var title 	= d.createElement('h3'),
	 		w_input = d.createElement('input'),
	 		w_update 	= d.createElement('u');
	 	title.innerHTML = 'Word: ';
	 	w_input.value = word;
	 	w_input.type = 'text';
	 	itemsContainer.append(title);
	 	title.append(w_input);
	 	w_input.classList.add('w_input');
	 	w_update.innerHTML = 'update word';
	 	w_update.setAttribute('onclick', 'wordNameUpdate(' + id + ')');
	 	title.append(w_update);
	 	w_update.classList.add('w_update');

	 })
	 .catch(err => c(err))

	fetch('/admin/api.php?_='+Math.random(1000)+'&action=sounds&id='+id+'&start='+start)
	 .then(res => res.json())
	 .then(res => {

	 	var text 		= res.data,
	 		start 		= parseInt(res.start),
	 		count 		= res.count,
	 		total 		= d.createElement('h4'),
	 		updateAll 	= d.createElement('h5');

	 	total.innerHTML = `Total: ${count}`;
	 	updateAll.innerHTML = 'update all';

	 	updateAll.setAttribute('onclick', 'd.querySelectorAll(".item.update a").forEach((el)=>el.click())');

	 	itemsContainer.append(total);
	 	itemsContainer.append(updateAll);

	 	if(start - 10 >= 0) {

	 		var iPrev 	= d.createElement('button'),
	 			prevPg  = start - 10;

			iPrev.setAttribute('onclick', `soundsEdit(${id}, ${prevPg})`);
			iPrev.innerHTML = '&larr; Previous';
			itemsContainer.append(iPrev);

	 	}

	 	if(start + 10 < count) {

	 		var iNext 	= d.createElement('button'),
	 			nextPg  = start + 10 <= count ? start + 10 : count - start;

			iNext.setAttribute('onclick', `soundsEdit(${id}, ${nextPg})`);
			iNext.innerHTML = 'Next &rarr;';
			itemsContainer.append(iNext);

	 	}

	 	for(var x = 0; x < text.length; x++) {

	 		itemRow = d.createElement('div');
	 		itemsContainer.appendChild(itemRow);

	 		text[x].update = '<a onclick=soundUpdate('+text[x].id+')>update</a><br>'


	 		Object.keys(text[x]).forEach(function (key) {

			   	item = _el('div', '', { 'class': 'item ' + key })
			   	itemRow.appendChild(item);

			   	//c(key)

			   	switch(key) {
			   		case 'sound_name': 
			   		item.innerHTML = '<input type="text" id="'+text[x].id+'" value="'+text[x][key]+'">';
			   		break;
			   		case 'sound_name_en': 
			   		item.innerHTML = '<input type="text" disabled value="'+text[x][key]+'">';
			   		break;
			   		case 'sound_url': 
			   		item.innerHTML = '<audio src="' + text[x][key] + '" controls></audio>';
			   		break;
			   		case 'img_url':
			   		item.innerHTML = '<img src="/' + text[x][key] + '">';
			   		break;
			   		default: item.innerHTML = text[x][key];
			   	}


			});

	 	}
	 })
	 .catch(err => c(err))
}


function soundUpdate(id) {
	
	var newName = d.getElementById(id).value,
		updLink = d.querySelectorAll('a[onclick="soundUpdate('+id+')"]')[0];

	fetch('/admin/api.php?_='+Math.random(1000)+'&action=update&id='+id+'&name='+newName)
	 .then(res => res.text())
	 .then(text => {
	 	updLink.style.color = text;
	 })
	 .catch(err => c(err))

}


function wordNameUpdate(id) {

	var newWord 	= d.querySelectorAll('.w_input')[0].value,
		w_update 	= d.querySelectorAll('u[onclick="wordNameUpdate('+id+')"]')[0];

	fetch('/admin/api.php?_='+Math.random(1000)+'&action=w_update&id='+id+'&word='+newWord)
	 .then(res => res.text())
	 .then(text => {
	 	w_update.style.color = text;
	 })
	 .catch(err => c(err))

}


function wordFullUpdate(keyword) {

	fetch('/admin/api.php?_='+Math.random(1000)+'&action=w_update_all&word='+keyword)
	 .then(res => res.text())
	 .then(text => {
	 	c(text);
	 })
	 .catch(err => c(err))

}

function wordDelete(id) {

	fetch('/admin/api.php?_='+Math.random(1000)+'&action=w_delete&id='+id)
	 .then(res => res.text())
	 .then(text => {
	 	// alert(text+' sounds deleted');
	 	console.log(text+' sounds deleted');
	 })
	 .catch(err => c(err))

}

function c(d) { return console.log(d) }

function _el (node, html, attributes) {
	var el
	if(node) 		el = d.createElement(node) 
	if(html) 		el.innerHTML = html
	if(attributes) 	for(key in attributes) { el.setAttribute(key, attributes[key]) }
	return el
}

function clickFromBreak() {

	for(let i = 1109; i < 1450; i++) { 
		document.querySelectorAll(".w_update_all")[i].click(); 
	}
}
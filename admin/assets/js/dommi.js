'use strict';

const 	d = document,
		e = Element.prototype;

d.ce = d.createElement,
d.qs = d.querySelector,
e.sa = e.setAttribute,
e.ga = e.getAttribute; 

export { d, e }
<?php

require_once '../dbconn.php';

if($_REQUEST['action']) { 

	if($_REQUEST['action'] === 'update') {

		$newName = urldecode($_REQUEST['name']);
		// $mysqli->set_charset('utf8');
		// $mysqli->query('SET NAMES cp1251');
		// $mysqli->query('SET CHARACTER SET cp1251');
		$query = 'update sounds set sound_name="'.$newName.'" where id="'.intval($_REQUEST['id']).'"';
		$result = $mysqli->query($query);
		if($result) echo 'lightgreen';
			else echo 'red';


		// echo json_encode([ 'name' => urldecode($_REQUEST['name']) ]);

		exit;

	} else if($_REQUEST['action'] === 'w_update') {

		$newWord = urldecode($_REQUEST['word']);
		$query = 'update searches set words="'.$newWord.'" where id="'.intval($_REQUEST['id']).'"';
		$result = $mysqli->query($query);
		if($result) echo 'lightgreen';
			else echo 'red';


		exit;

	} else if($_REQUEST['action'] === 'w_delete') {

			$mysqli->query('delete from searches where id='.intval($_REQUEST['id']));
			$mysqli->query('delete from sounds where search_id='.intval($_REQUEST['id']));
			echo $mysqli->affected_rows;
			exit;

	} else if($_REQUEST['action'] === 'w_update_all') {

		$word = str_replace(' ', '+', $_REQUEST['word']);

		$res_id = $mysqli->query('select id from searches where words="'.$_REQUEST['word'].'"');
		$search_id = mysqli_fetch_row($res_id)[0];
		$mysqli->query('delete from searches where sound_name='.$_REQUEST['word']);
		$mysqli->query('delete from sounds where search_id='.$search_id);

		$ya_answ = file_get_contents('https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20141220T185811Z.faeeb6f26ab48a3f.9990226aa13dc1833b4dae2fc7567431e762f7b4&text='.$word.'&lang=ru-en');
        preg_match_all('#text>(.+)</text#iU', $ya_answ, $n);
        $keywords = str_replace(' ','+',$n[1][0]);
        $fs_page = file_get_contents('http://findsounds.com/ISAPI/search.dll?keywords='.$keywords);

        $mysqli->query('update searches set words_en="'.str_replace('+', ' ', $keywords).'" where words="'.$_REQUEST['word'].'"') or die($mysqli->error);

        $page_count = substr_count($fs_page, 'advance-table-number');

        $url_updated = Array();

        for($page = 0; $page <= $page_count; $page++) {

        	$start = $page * 10 + 1;

        	$fs_page = file_get_contents('http://findsounds.com/ISAPI/search.dll?start='.$start.'&keywords='.$keywords);

        	$raw_hits1 = substr($fs_page, strpos($fs_page, '>hit') + 1);
	        $raw_hits2 = substr($raw_hits1, 0, strpos($raw_hits1, '</'));
	        $raw_hits3 = explode('hit(', str_replace('"', '', $raw_hits2));

	        foreach ($raw_hits3 as $i => $raw_hit) {

	        	$hit = explode(',', $raw_hit);

	        	$ya_answ = file_get_contents('https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20141220T185811Z.faeeb6f26ab48a3f.9990226aa13dc1833b4dae2fc7567431e762f7b4&text='.urlencode($hit[7]).'&lang=en-ru');
		        preg_match_all('#text>(.+)</text#iU', $ya_answ, $n);

		        $sound_name_ru  = $n[1][0];
	        	$sound_name_en 	= $hit[7];
	        	$sound_url		= $hit[4];
	        	$img_url		= 'img/sounds/'.$hit[3].'.jpg';
	        	$size			= $hit[8];
	        	$length			= $hit[12];
	        	$bits			= $hit[10];
	        	$frequency		= $hit[11];

	        	if($sound_url === NULL || !is_numeric($size) || in_array($sound_url, $url_updated)) continue;

	        	$url_updated[] = $sound_url;

	        	$query = 'insert into sounds (search_id, sound_name, sound_name_en, sound_url, img_url, size, length, bits, frequency) values ("'.$search_id.'","'.$sound_name_ru.'","'.$sound_name_en.'","'.$hit[4].'","'.$img_url.'","'.$hit[8].'",'.$hit[12].',"'.$hit[10].'","'.$hit[11].'")';
				$mysqli->query($query) or die($mysqli->error);

				copy('http://findsounds.com/images/waveform/'.$hit[3].'.jpg','../img/sounds/'.$hit[3].'.jpg');
	        }

        }

        echo 'Ok, '.$page_count.' pages updated, urls array length: '.count($url_updated);
        exit;


	} else if($_REQUEST['action'] === 'words') {

		$result = $mysqli->query('select * from searches');
		while($r = mysqli_fetch_assoc($result)) {

			$rows[] = $r;
		}
		echo json_encode([ 'data' 	=> $rows ], JSON_UNESCAPED_SLASHES);


	} else if($_REQUEST['action'] === 'word') {

		$result = $mysqli->query('select words from searches where id="'.$_REQUEST['id'].'"');
		echo mysqli_fetch_row($result)[0];


	} else if($_REQUEST['action'] === 'sounds') {

		$start = $_REQUEST['start'];
		$countRes = $mysqli->query('select count(*) from sounds where search_id="'.$_REQUEST['id'].'"');
		$count = mysqli_fetch_row($countRes)[0];
		$query = 'select id, sound_name, sound_name_en, sound_url, img_url, size, length, bits, frequency from sounds where search_id="'.$_REQUEST['id'].'" limit '.$start.',10';
		$result = $mysqli->query($query);
		if($result && $result->num_rows !== 0) {
			while($r = mysqli_fetch_assoc($result)) {
				$rows[] = $r;
			}
			echo json_encode([
				'data' 	=> $rows,
				'start' => $start,
				'count' => $count,
			], JSON_UNESCAPED_SLASHES);
		} else echo json_encode(['count' => 0, 'id' => $_REQUEST['id']]);

	}

}

?>
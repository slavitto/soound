import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
// import {tap} from "rxjs/operators";

export interface Serp {

  id: number,
  sound_name: string,
  sound_name_en: string,
  sound_url: string,
  img_url: string,
  size: number,
  length: number,
  bits: number,
  frequency: number
}

@Injectable({ providedIn: "root"})

export class SerpService {

  public serpItems: Serp[] = [];

  constructor(private http: HttpClient) {}

  searchHandler(keyword: string = ''): Observable<Serp[]> {
    // return this.http.get<Serp[]>('https://my-json-server.typicode.com/slavitto/soound-fake-api/data')
    return this.http.get<Serp[]>('http://soound.local/api.php?action=search&keyword=' + keyword)
      // .pipe(tap(serpItems => this.serpItems = serpItems))
  }

}

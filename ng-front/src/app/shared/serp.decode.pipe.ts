import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: 'DecodePipe'
})

export class SerpDecodePipe implements PipeTransform {

  transform(sound_name: string) {
    return decodeURIComponent(sound_name)
  }

}

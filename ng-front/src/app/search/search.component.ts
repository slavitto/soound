import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {SerpService} from '../shared/serp.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  keyword = '';
  language: number = 1;

  constructor(public serpService: SerpService) { }

  @Output() onChange = new EventEmitter<number>();


  ngOnInit() {
  }

  doSearch(event: any) {
    if(event.code !== 'Enter' && event.target.id !== 'searchBtn') return false;
    event.target.blur();
    this.serpService.searchHandler(this.keyword).subscribe(serp => {
      this.serpService.serpItems = serp;
    });
  }

  changeLanguage() {
    this.onChange.emit(this.language);
  }


}

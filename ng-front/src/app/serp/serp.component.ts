import {Component, Input, OnInit, Output} from '@angular/core';
import {SerpService} from "../shared/serp.service";

@Component({
  selector: 'app-serp',
  templateUrl: './serp.component.html',
  styleUrls: ['./serp.component.scss']
})

export class SerpComponent implements OnInit {

  @Input() advert: string;

  constructor(public serpService: SerpService) { }

  ngOnInit() {
  }

}

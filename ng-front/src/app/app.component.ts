import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  appHeader = 'SOOUND - все звуки мира';
  advert: string = 'advert';

  onChange(language: number) {
    this.appHeader = language == 2 ? 'SOOUND - all sounds of the world' : 'SOOUND - все звуки мира';
  }
}

function playSound(s_id, pw, t) {
	var params = s_id + ',' + pw + ',' + t;
	document.getElementById("sound" + s_id).play();	
	document.getElementById("slider_" + s_id).style.visibility = "visible";
	document.getElementById("play_" + s_id).innerHTML = "<img id=b_" + s_id + " onclick=stopSound(" + params + ") src=img/stop.png>";
	setTimeout(() => document.getElementById("play_" + s_id).innerHTML = "<img id=b_" + s_id + " onclick=playSound("+ params +") src=img/play.png>", t*1000);
	slide(s_id, t);
} 

function stopSound(s_id, pw, t) {
	var params = s_id + ',' + pw + ',' + t;
	document.getElementById("sound" + s_id).pause();	
	document.getElementById("play_" + s_id).innerHTML = "<img id=b_" + s_id + " onclick=playSound(" + params + ") src=img/play.png>";
	document.getElementById("slider_" + s_id).style.visibility = "hidden";
	document.getElementById("sound" + s_id).currentTime = 0;
}

function slide(s_id, t, start) {
	start = Date.now();
	var timer = setInterval(function() {
		var timePassed = Date.now() - start;
		if (timePassed >= t*1000) {
			clearInterval(timer); 
			return;
		}	
		document.getElementById("slider_" + s_id).style.marginLeft = timePassed / 25 + 'px';
	}, 20);
}
